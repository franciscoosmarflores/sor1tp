#include <stdio.h>
#include <stdlib.h>
#include <pthread.h> 
#include <semaphore.h>
#include <unistd.h>

sem_t palillo[6];

void do_nothing(int seg, char* mensaje){
	printf("\n %s \n", mensaje);
	//sleep(seg); 
}
void *filosofo(int i) 
{
	while(1)
	{		
		printf("\n Soy el filosofo %d: Pienso\n",i);
		sem_wait(&palillo[i]);
		do_nothing(2,"Eureka!");
		printf("\nSoy el Filósofo %d : Agarrando palillo izquierdo\n",i);
		if(i == 5)
		{
			sem_wait(&palillo[1]);
		}
		else
		{
			sem_wait(&palillo[(i+1)]);
		}
		
		printf("Soy el Filósofo %d : Agarrando palillo derecho\n",i);
		
		//do_nothing(2,"Provecho ");//usleep(50000);
		
		printf("Soy el filosofo %d: Comiendo\n",i);
		sem_post(&palillo[i]);
		if(i == 5)
		{
			sem_post(&palillo[1]);
		}
		else
		{
			sem_post(&palillo[(i+1)]);
		}
		do_nothing(2,"Provecho!");
		if (i==5){
		exit(0);
	}			
		break;
	}	
}		

int main(){
	sem_init(&palillo[1],0,1);		
	sem_init(&palillo[2],0,1);		
	sem_init(&palillo[3],0,1);		
	sem_init(&palillo[4],0,1);		
	sem_init(&palillo[5],0,1);		
	
	pthread_t filosofos1;
	pthread_t filosofos2;
	pthread_t filosofos3;
	pthread_t filosofos4;
	pthread_t filosofos5;

	pthread_create(&filosofos1, NULL, filosofo(1),NULL);	
	pthread_create(&filosofos2, NULL, filosofo(2),NULL);	
	pthread_create(&filosofos3, NULL, filosofo(3),NULL);		
	pthread_create(&filosofos4, NULL, filosofo(4),NULL);		
	pthread_create(&filosofos5, NULL, filosofo(5),NULL);			
		
	pthread_join(filosofos1,NULL);
	pthread_join(filosofos2,NULL);
	pthread_join(filosofos3,NULL);	
	pthread_join(filosofos4,NULL);
	pthread_join(filosofos5,NULL);
}

