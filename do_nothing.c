#include <stdio.h>  //incluimos la libreria de estandar input/output
#include <unistd.h> //para hacer sleep
#include <stdlib.h> //para libreria de numeros random: srand, rand 
#include <time.h>   //para inicializar el tiempo
#include <sys/wait.h>//lib para utilizar el wait
void do_nothing(int microseconds, char* mensaje){
  usleep(microseconds);
  printf("\n %s \n",mensaje);   
}
void do_nothing_random(char* mensaje){
  srand(time(NULL));
  int microseconds =10000;
//rand() % 1000 + 1;
  usleep(microseconds);
  printf("\n %s \n",mensaje);
}
int main() {
	int childpid = fork();
	
	char* msg= "hola";
	if (childpid!=0)
	{
		 do_nothing(2000000,msg);
		wait(NULL);
	}
	else
	{ 
		
		do_nothing(2000000,msg);
	}
		return 0;
}
