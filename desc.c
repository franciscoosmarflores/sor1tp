#include <stdio.h>
#include <unistd.h>
#include <semaphore.h>
#include <sys/sem.h>
#include <pthread.h>
#include <stdlib.h>
int tiempo=200000;               
int i = 1;
int cant;
sem_t Sth1;
sem_t Sth2;
sem_t Sth3;
sem_t Sth4;
sem_t Sth5;
sem_t Sth6;
sem_t Sth7; 

void *thread1()
{
	while(1)
	{
		sem_wait(&Sth1);
		printf("\n Pienso \n");
		usleep(tiempo);
		sem_post(&Sth2);
		sem_post(&Sth3);		
		sem_post(&Sth4);
	}	
	
}
void *thread2()
{
	while(1)
	{
		sem_wait(&Sth2);
		printf("Mientras lavo los platos,");
		usleep(tiempo);
	}
}
void *thread3()
{
	while(1)
	{
		sem_wait(&Sth3);
		printf(" Mientras limpio el piso,");
		usleep(tiempo);
	}
}
void *thread4()
{
	while(1)
	{
		sem_wait(&Sth4);
		printf(" Mientras riego las plantas ");
		usleep(tiempo);
		sem_post(&Sth5);
	}
}
void *thread5()
{
	while(1)
	{
		sem_wait(&Sth5);
		printf("\n Existo ");
		usleep(tiempo);
		sem_post(&Sth6);
	}
}
void *thread6()
{
	while(1)
	{
		sem_wait(&Sth6);
		printf("\n Hablar");
		usleep(tiempo);
		sem_post(&Sth7);
	}
}
void *thread7(int cant)
{
	
	while(1)
	{
		sem_wait(&Sth7);
		printf("\n Tomar una decision\n");
		usleep(tiempo);
		if (i<cant)
		{		
			sem_post(&Sth1);
			i++;
		}
		else
		{
			exit(0);		
		}
	}
}
	
int main()
{
	printf("cuantas veces desea repetir \n");
	scanf("%i",&cant);	

	pthread_t t1,t2,t3,t4,t5,t6,t7;
	int sem1 = sem_init(&Sth1,0,1);
	int sem2 = sem_init(&Sth2,0,0);
	int sem3 = sem_init(&Sth3,0,0);
	int sem4 = sem_init(&Sth4,0,0);
	int sem5 = sem_init(&Sth5,0,0);
	int sem6 = sem_init(&Sth6,0,0);
	int sem7 = sem_init(&Sth7,0,0);
	
	int vth1 = pthread_create(&t1, NULL, thread1,NULL);
	int vth2 = pthread_create(&t2, NULL, thread2,NULL);
	int vth3 = pthread_create(&t3, NULL, thread3,NULL);
	int vth4 = pthread_create(&t4, NULL, thread4,NULL);
	int vth5 = pthread_create(&t5, NULL, thread5,NULL);
	int vth6 = pthread_create(&t6, NULL, thread6,NULL);
	int vth7 = pthread_create(&t7, NULL, thread7(cant),NULL);
	
	pthread_join(t1,NULL);
	pthread_join(t2,NULL);
	pthread_join(t3,NULL);
	pthread_join(t4,NULL);
	pthread_join(t5,NULL);
	pthread_join(t6,NULL);
	pthread_join(t7,NULL);


}
